<?php

/**
 * @file
 * Hook implementations for the Paddle Classic theme.
 */

/**
 * Implements hook_paddle_themer_style_set().
 */
function paddle_classic_theme_paddle_themer_style_set() {
  $style_sets = array();

  $style_sets['global'] = array(
    'title' => t('Global styles'),
    'selectors' => array(
      'h1' => array(
        'title' => t('H1 heading'),
        'plugins' => array(
          array(
            'machine_name' => 'font',
            'title' => 'Font',
            'allowed_values' => array(
              'font_family' => array('times new roman', 'times', 'serif'),
              'font_size' => array('largest', 'very large', 'large'),
            ),
          ),
          array(
            'machine_name' => 'color',
            'title' => 'Color',
          ),
        ),
      ),
      'h2' => array(
        'title' => t('H2 heading'),
        'plugins' => array(),
      ),
      'h3' => array(
        'title' => t('H3 heading'),
        'plugins' => array(),
      ),
      'p' => array(
        'title' => t('Paragraph'),
        'plugins' => array(),
      ),
      'blockquote' => array(
        'title' => t('Quote'),
        'plugins' => array(),
      ),
      'ul, ul li, ol, ol li' => array(
        'title' => t('List'),
        'plugins' => array(),
      ),
      'a' => array(
        'title' => t('Link'),
        'plugins' => array(),
      ),
      'body' => array(
        'title' => t('Body'),
        'plugins' => array(
          array(
            'machine_name' => 'background',
            'title' => 'Background',
            'allowed_values' => array(
              'background_transparent' => FALSE,
              'background_color' => TRUE,
            ),
          ),
        ),
      ),
    ),
  );

  return $style_sets;
}
